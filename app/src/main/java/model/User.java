package model;

import com.google.gson.annotations.Expose;

import rest.model.LoginResponse;

public class User extends LoginResponse {

    @Expose
    private String password;

    /**
     * @return The password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param Password The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

}
