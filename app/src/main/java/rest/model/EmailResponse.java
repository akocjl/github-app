package rest.model;

import com.google.gson.annotations.Expose;

public class EmailResponse {

    @Expose
    private String email;
    @Expose
    private Boolean verified;
    @Expose
    private Boolean primary;

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The verified
     */
    public Boolean getVerified() {
        return verified;
    }

    /**
     * @param verified The verified
     */
    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    /**
     * @return The primary
     */
    public Boolean getPrimary() {
        return primary;
    }

    /**
     * @param primary The primary
     */
    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

}