package rest.service;

import java.util.List;

import model.Email;
import model.Repository;
import model.User;
import rest.model.EmailResponse;
import rest.model.LoginResponse;
import rest.model.RepositoryResponse;
import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Header;

public interface GitHubService {

    @GET("/zen")
    void test(Callback<Response> cb);

    @GET("/user")
    void login(@Header("Authorization") String authorization, Callback<User> cb);

    @GET("/user/emails")
    void getEmails(@Header("Authorization") String authorization, Callback<List<Email>> cb);

    @GET("/user/repos")
    void getRepositories(@Header("Authorization") String authorization, Callback<List<Repository>> cb);
}
