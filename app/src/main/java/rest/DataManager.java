package rest;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.util.List;

import model.Email;
import model.Repository;
import model.User;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DataManager {

    private static DataManager instance;
    private RestClient restClient;
    private User userProfile;

    public interface LoginCallbacks {
        public void onLoginSuccess(User user);
        public void onLoginFailed();
    }

    public interface EmailCallbacks {
        public void onEmailsDownloaded(List<Email> emails);
    }

    public interface RepositoryCallbacks {
        public void onRepositoriesDownloaded(List<Repository> repositories);
        public void onRepositoriesDownloadError(RetrofitError error);
    }

    private DataManager() {
        restClient = new RestClient();
    }

    public static DataManager getInstance() {
        if (instance == null)
            instance = new DataManager();

        return instance;
    }

    public User getUserProfile(){
        return this.userProfile;
    }

    public void logIn(String username, final String password, final LoginCallbacks cb) {
        String auth = generateAuthToken(username, password);
        restClient.getService().login(auth, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                userProfile = user;
                userProfile.setPassword(password);
                cb.onLoginSuccess(userProfile);
            }

            @Override
            public void failure(RetrofitError error) {
                // TODO: handle errors
                cb.onLoginFailed();
            }
        });
    }

    public void getEmails(final EmailCallbacks cb) {
        String auth = generateAuthToken(userProfile.getLogin(), userProfile.getPassword());
        restClient.getService().getEmails(auth, new Callback<List<Email>>() {
            @Override
            public void success(List<Email> emails, Response response) {
                cb.onEmailsDownloaded(emails);
            }

            @Override
            public void failure(RetrofitError error) {
                // TODO: handle errors
            }
        });
    }

    public void getRepositories(final RepositoryCallbacks cb) {
        String auth = generateAuthToken(userProfile.getLogin(), userProfile.getPassword());
        restClient.getService().getRepositories(auth, new Callback<List<Repository>>() {
            @Override
            public void success(List<Repository> repositories, Response response) {
                cb.onRepositoriesDownloaded(repositories);
            }

            @Override
            public void failure(RetrofitError error) {
                cb.onRepositoriesDownloadError(error);
            }
        });
    }

    private String generateAuthToken(String username, String password) {
        String userPass = username + ":" + password;
        byte[] data = new byte[0];
        try {
            data = userPass.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.DEFAULT);
    }
}