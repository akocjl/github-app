package rest;

import rest.service.GitHubService;
import retrofit.RestAdapter;

public class RestClient {

    private static final String BASE_URL = "https://api.github.com";
    private static GitHubService gitHubservice;

    public RestClient() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        gitHubservice = restAdapter.create(GitHubService.class);
    }

    public GitHubService getService() {
        return gitHubservice;
    }
}
