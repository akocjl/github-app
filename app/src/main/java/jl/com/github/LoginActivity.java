package jl.com.github;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import model.Email;
import model.Repository;
import model.User;
import rest.DataManager;
import rest.RestClient;
import rest.model.ErrorResponse;
import rest.model.LoginResponse;
import rest.model.RepositoryResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import utils.Device;
import utils.IO;


public class LoginActivity extends Activity implements DataManager.LoginCallbacks {
    private static final String LOG_TAG = "gitHub_login";

    private View loginForm, progressView;
    private EditText usernameView, passwordView;
    private Button signInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginForm = findViewById(R.id.loginForm);
        progressView = findViewById(R.id.progressBar);
        usernameView = (EditText) findViewById(R.id.usernameView);
        passwordView = (EditText) findViewById(R.id.passwordView);
        passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        signInButton = (Button) findViewById(R.id.signInButton);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
    }

    @Override
    public void onLoginSuccess(User user) {
        Log.i(LOG_TAG, user.getLogin());

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onLoginFailed() {
        showProgress(false);
        Toast.makeText(this, "Incorrect username or password", Toast.LENGTH_LONG).show();
    }

    private void attemptLogin() {
        String username = usernameView.getText().toString();
        String password = passwordView.getText().toString();

        if (TextUtils.isEmpty(username)) {
            usernameView.setError(getString(R.string.error_field_required));
            usernameView.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            passwordView.setError(getString(R.string.error_field_required));
            passwordView.requestFocus();
            return;
        }

        hideKeyboard();

        if (!Device.isConnected(this)) {
            Toast.makeText(this, "Internet connection is required", Toast.LENGTH_SHORT).show();
            return;
        }

        showProgress(true);
        DataManager.getInstance().logIn(username, password, this);
    }

    private void showProgress(boolean show) {
        progressView.setVisibility(show ? View.VISIBLE : View.GONE);
        loginForm.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }
}
