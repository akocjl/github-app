package jl.com.github;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

import jl.com.github.adapters.RepositoryAdapter;
import jl.com.github.ui.custom.ButteryProgressBar;
import jl.com.github.ui.custom.CountView;
import model.Email;
import model.Repository;
import model.User;
import rest.DataManager;
import retrofit.RetrofitError;
import utils.Device;

public class MainActivity extends ActionBarActivity implements DataManager.EmailCallbacks, DataManager.RepositoryCallbacks {
    private static final String LOG_TAG = "gitHub_main";

    private User userProfile;
    private ImageView avatarImage;
    private TextView nameText, usernameText;
    private TextView companyText, emailText;
    private CountView publicRepoView, privateRepoOwnedView, privateRepoTotalView;
    private CountView publicGistView, privateGistView;
    private ButteryProgressBar progressBar;

    private DrawerLayout drawer;
    private View drawerView;
    private ActionBarDrawerToggle drawerToggle;

    private ListView repoListView;
    private TextView emptyView;
    private RepositoryAdapter repoAdapter;

    private MenuItem refreshItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpToolBar();

        repoListView = (ListView) findViewById(android.R.id.list);
        emptyView = (TextView) findViewById(android.R.id.empty);
        progressBar = (ButteryProgressBar) findViewById(R.id.progressBar);
        repoListView.setEmptyView(emptyView);

        // drawer views
        avatarImage = (ImageView) findViewById(R.id.avatarImage);
        nameText = (TextView) findViewById(R.id.nameText);
        usernameText = (TextView) findViewById(R.id.usernameText);
        companyText = (TextView) findViewById(R.id.companyText);
        emailText = (TextView) findViewById(R.id.emailText);
        publicRepoView = (CountView) findViewById(R.id.publicRepoView);
        privateRepoOwnedView = (CountView) findViewById(R.id.privateRepoOwnedView);
        privateRepoTotalView = (CountView) findViewById(R.id.privateRepoTotalView);
        publicGistView = (CountView) findViewById(R.id.publicGistView);
        privateGistView = (CountView) findViewById(R.id.privateGistView);

        userProfile = DataManager.getInstance().getUserProfile();
        initDrawer(userProfile);

        DataManager.getInstance().getEmails(this);
        DataManager.getInstance().getRepositories(this);
    }

    private void setUpToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GitHub Repositories");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerView = findViewById(R.id.left_drawer);

        drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.app_name, R.string.app_name);
        drawer.setDrawerListener(drawerToggle);
    }

    private void initDrawer(User profile) {
        // set basic profile values
        nameText.setText(profile.getName());
        usernameText.setText(profile.getLogin());
        companyText.setText(TextUtils.isEmpty(profile.getCompany()) ? "Not Set" : profile.getCompany());
        Picasso.with(this).load(profile.getAvatarUrl()).placeholder(R.drawable.avatar_github).into(avatarImage);

        // set repository count values
        publicRepoView.setCount(profile.getPublicRepos());
        privateRepoOwnedView.setCount(profile.getOwnedPrivateRepos());
        privateRepoTotalView.setCount(profile.getTotalPrivateRepos());

        // set gist count values
        publicGistView.setCount(userProfile.getPublicGists());
        privateGistView.setCount(userProfile.getPrivateGists());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        refreshItem = menu.findItem(R.id.action_refresh);
        refreshItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                if (Device.isConnected(getApplicationContext())) {
                    DataManager.getInstance().getRepositories(MainActivity.this);
                    showProgress(true);
                } else {
                    Toast.makeText(MainActivity.this, "Internet connection is required", Toast.LENGTH_SHORT).show();
                }

                return true;
            }
        });
        refreshItem.getIcon().setAlpha(90);

        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(drawerView))
            drawer.closeDrawer(drawerView);
        else
            super.onBackPressed();
    }

    @Override
    public void onEmailsDownloaded(List<Email> emails) {
        StringBuilder sb = new StringBuilder();
        for (Email e : emails) {
            sb.append(e.getEmail());
            sb.append("\n");
        }
        emailText.setText(sb.toString().trim());
    }

    @Override
    public void onRepositoriesDownloaded(List<Repository> repositories) {
        showProgress(false);
        repoAdapter = new RepositoryAdapter(this, repositories);
        repoListView.setAdapter(repoAdapter);
        repoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Repository item = repoAdapter.getItem(i);
                goToDetailsActivity(item);
            }
        });

        if (repositories.size() == 0)
            emptyView.setText(R.string.message_no_repositories_found);
    }

    @Override
    public void onRepositoriesDownloadError(RetrofitError error) {
        showProgress(false);
        Toast.makeText(this, "Failed to load repositories", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    private void showProgress(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        refreshItem.setEnabled(!show);
        refreshItem.getIcon().setAlpha(show ? 90 : 255);
    }


    private void goToDetailsActivity(Repository repo) {
        String repoStr = new Gson().toJson(repo);
        Intent detailsActivity = new Intent(this, DetailsActivity.class);
        detailsActivity.putExtra("repo", repoStr);
        startActivity(detailsActivity);
    }
}
