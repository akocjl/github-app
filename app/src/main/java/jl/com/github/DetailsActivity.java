package jl.com.github;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import java.text.DateFormat;
import java.util.Date;

import jl.com.github.ui.custom.LabelValueView;
import model.Repository;

public class DetailsActivity extends ActionBarActivity {

    Repository repo;
    LinearLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        setUpToolBar();

        mainLayout = (LinearLayout) findViewById(R.id.mainLayout);

        String repoStr = getIntent().getStringExtra("repo");
        repo = new Gson().fromJson(repoStr, Repository.class);
        getSupportActionBar().setTitle(repo.getName());

        addView("id:", String.valueOf(repo.getId()));
        addView("full name:", repo.getFullName());
        addView("last commit:", repo.getPushedAt());
        addView("private:", String.valueOf(repo.getPrivate()));
        addView("has wiki:", String.valueOf(repo.getHasWiki()));
        addView("watchers count:", String.valueOf(repo.getWatchers()));
        addView("stars count:", String.valueOf(repo.getStargazersCount()));
        addView("git url:", repo.getGitUrl());
        addView("ssh url:", repo.getSshUrl());
        addView("clone url:", repo.getCloneUrl());
        addView("svn url:", repo.getSvnUrl());
    }

    private void setUpToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    private void addView(String label, String value) {
        LabelValueView v = new LabelValueView(this);
        v.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        v.setLabel(label);
        v.setValue(value);

        mainLayout.addView(v);
    }
}
