package jl.com.github.ui.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import jl.com.github.R;

public class LabelValueView extends LinearLayout {

    TextView labelText;
    TextView valueText;

    public LabelValueView(Context context) {
        super(context, null);
        inflateLayout(context);
    }

    public LabelValueView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflateLayout(context);
    }

    private void inflateLayout(Context context) {
        LayoutInflater.from(context).inflate(R.layout.custom_label_value_view, this, true);
        labelText = (TextView) findViewById(R.id.labelText);
        valueText = (TextView) findViewById(R.id.valueText);
    }

    public void setLabel(CharSequence text) {
        labelText.setText(text);
    }

    public void setValue(CharSequence text) {
        valueText.setText(text);
    }
}
