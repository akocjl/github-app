package jl.com.github.ui.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import jl.com.github.R;

public class CountView extends LinearLayout{

    TextView labelText;
    TextView countText;

    public CountView(Context context) {
        super(context, null);
        inflateLayout(context);
    }

    public CountView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflateLayout(context);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CountView, 0, 0);
        labelText.setText(a.getString(R.styleable.CountView_label));
    }

    private void inflateLayout(Context context){
        LayoutInflater.from(context).inflate(R.layout.custom_count_view, this, true);
        labelText = (TextView) findViewById(R.id.labelText);
        countText = (TextView) findViewById(R.id.countText);
    }

    public void setCount(int count){
        countText.setText(String.valueOf(count));
    }

}
