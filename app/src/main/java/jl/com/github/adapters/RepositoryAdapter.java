package jl.com.github.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import jl.com.github.R;
import model.Repository;

public class RepositoryAdapter extends ArrayAdapter<Repository> {

    public RepositoryAdapter(Context context, List<Repository> objects) {
        super(context, 0, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Repository repo = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item_repository, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.repositoryNameText);
            viewHolder.description = (TextView) convertView.findViewById(R.id.descriptionText);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Populate the data into the template view using the data object
        String desc = "No description available.";
        if (!TextUtils.isEmpty(repo.getDescription()))
            desc = repo.getDescription();

        viewHolder.description.setText(desc);
        viewHolder.name.setText(repo.getFullName());

        return convertView;
    }

    private static class ViewHolder {
        TextView name;
        TextView description;
    }
}
